import java.util.Scanner;

public class Application{
	public static void main(String args[]){	
		Scanner reader = new Scanner(System.in);
	
		Student[] section3 = new Student[3];
		
		//Student marco = new Student();
		//Student maeva = new Student();
		
		Student newStud = new Student(7, "brown");
		newStud.setRScore(31.000);
		System.out.println(newStud.getRScore());
		System.out.println(newStud.getNumOfClasses());
		System.out.println(newStud.getEyeColour());	
		System.out.println(newStud.amountLearnt);	
		
		/*
		section3[0] = marco;
		section3[1] = maeva;
		section3[2] = new Student();
		
		section3[2].setRScore(30.119);
		section3[2].setNumOfClasses(5);
		System.out.println(section3[2].getEyeColour());				
		section3[2].setEyeColour("brown");
		System.out.println(section3[2].getEyeColour());	
		
		marco.setRScore(31.779);
		marco.setNumOfClasses(5);
		marco.setEyeColour("green");
		
		maeva.setRScore(35.000);
		maeva.setNumOfClasses(7);
		maeva.setEyeColour("brown");
		
		System.out.println(section3[2].getRScore());
		System.out.println(section3[2].getNumOfClasses());
		System.out.println(section3[2].getEyeColour());		
		
		System.out.println("How much did you study?");
		int amountStudied = reader.nextInt();
		
		section3[2].study(amountStudied);
		
		System.out.println(section3[0].getRScore());
		System.out.println(section3[0].getNumOfClasses());
		System.out.println(section3[0].getEyeColour());	
		System.out.println(section3[0].amountLearnt);		
		System.out.println(section3[1].amountLearnt);		
		System.out.println(section3[2].amountLearnt);		
				
		//marco.blink();
		//maeva.blink();
		*/
	}
}