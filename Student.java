public class Student{
	private double rScore;
	private int numOfClasses;
	private String eyeColour;	
	public int amountLearnt;
	
	public Student(int numOfClasses, String eyeColour){
		this.rScore = 30.000;
		this.numOfClasses = numOfClasses;
		this.eyeColour = eyeColour;
		this.amountLearnt = 0; 
	}
	
	public void fullTime(){
		if(this.numOfClasses > 3){
			System.out.println("You are a full time student!");
		} else{
			System.out.println("You are not a full time student");
		}
	}
	
	public void blink(){
		System.out.println("You elegantly close your eyes for an instant to then reaveal once again your beautiful " + this.eyeColour + " eyes");
	}
	
	public void study(int amountStudied){
		this.amountLearnt += amountStudied;
	}
	
	public double getRScore(){
		return this.rScore;
	}
	public int getNumOfClasses(){
		return this.numOfClasses;
	}
	public String getEyeColour(){
		return this.eyeColour;
	}
	
	public void setRScore(double rScore){
		this.rScore = rScore;
	}
}